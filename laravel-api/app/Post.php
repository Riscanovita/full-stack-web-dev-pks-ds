<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'description'];

    protected $keyType = 'string';
    
    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating(function($model){
         $model->id = Str::uuid();

        });
    }

    public function Comment()
    {
        return $this->hasMany('App\Comment');
    }
}
