<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table posts
        $role = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Role',
            'data'    => $role  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find post by ID
        $role = Role::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data'    => $role
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([
            'name'     => $request->name
        ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $role
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $post
     * @return void
     */
    public function update(Request $request, Role $role)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $role = Role::findOrFail($role->id);

        if($role) {

            //update post
            $role->update([
                'name'     => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $role 
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find post by ID
        $role = Role::findOrfail($id);

        if($role) {

            //delete post
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}