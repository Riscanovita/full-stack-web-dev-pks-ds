<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Carbon\Carbon;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'email'   => 'required',    
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email',$request->email)->first();
        if (!$user->otpCode){
            $user->OtpCode->delete();
        }
        
        //regenerate ulang
        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while($check);

        $validUntil = Carbon::now()->addMinutes(15);
        $otp_code = OtpCode::create([
            'otp'         => $otp,
            'valid_until' => $validUntil,
            'user_id'     => $user->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'OTP telah diregenerasi. Silahkan Periksa Email',
            'data'    => [
                'user'     => $user,
                'otp_code' => $otp_code
            ]
        ]);

    }
}
